import _ from 'lodash';
import sweph from 'sweph';
import path from 'path';
import { fileURLToPath } from 'url';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

sweph.set_ephe_path(path.join(__dirname, './ephemeris'));
const SC = sweph.constants;

const aster = {};

aster.PLANETS = [
    { id: SC.SE_SUN, name: "Sun", unicode: "☉", class: "luminary" },
    { id: SC.SE_MOON, name: "Moon", unicode: "☾", class: "luminary" },
    { id: SC.SE_MERCURY, name: "Mercury", unicode: "☿", class: "personal" },
    { id: SC.SE_VENUS, name: "Venus", unicode: "♀", class: "personal" },
    { id: SC.SE_EARTH, name: "Earth", unicode: "♁", class: "other" },
    { id: SC.SE_MARS, name: "Mars", unicode: "♂", class: "personal" },
    { id: SC.SE_JUPITER, name: "Jupiter", unicode: "♃", class: "social" },
    { id: SC.SE_SATURN, name: "Saturn", unicode: "♄", class: "social" },
    { id: SC.SE_URANUS, name: "Uranus", unicode: "♅", class: "transpersonal" },
    { id: SC.SE_NEPTUNE, name: "Neptune", unicode: "♆", class: "transpersonal" },
    { id: SC.SE_PLUTO, name: "Pluto", unicode: "♇", class: "transpersonal" },
    { id: SC.SE_MEAN_NODE, name: "Mean Node", unicode: "☊", class: "other" },
    { id: SC.SE_TRUE_NODE, name: "True Node", unicode: "☊", class: "other" },
    { id: SC.SE_CHIRON, name: "Chiron", class: "other" },
    { id: SC.SE_PHOLUS, name: "Pholus", class: "other" },
    { id: SC.SE_CERES, name: "Ceres", class: "other" },
    { id: SC.SE_PALLAS, name: "Pallas", class: "other" },
    { id: SC.SE_JUNO, name: "Juno", class: "other" },
    { id: SC.SE_VESTA, name: "Vesta", class: "other" },
];

aster.SIGNS = [
    { id: 1, name: "Aries", unicode: "♈", element: "fire", mode: "cardinal" },
    { id: 2, name: "Taurus", unicode: "♉", element: "earth", mode: "fixed" },
    { id: 3, name: "Gemini", unicode: "♊", element: "air", mode: "mutable" },
    { id: 4, name: "Cancer", unicode: "♋", element: "water", mode: "cardinal" },
    { id: 5, name: "Leo", unicode: "♌", element: "fire", mode: "fixed" },
    { id: 6, name: "Virgo", unicode: "♍", element: "earth", mode: "mutable" },
    { id: 7, name: "Libra", unicode: "♎", element: "air", mode: "cardinal" },
    { id: 8, name: "Scorpio", unicode: "♏", element: "water", mode: "fixed" },
    { id: 9, name: "Sagittarius", unicode: "♐", element: "fire", mode: "mutable" },
    { id: 10, name: "Capricorn", unicode: "♑", element: "earth", mode: "cardinal" },
    { id: 11, name: "Aquarius", unicode: "♒", element: "air", mode: "fixed" },
    { id: 12, name: "Pisces", unicode: "♓", element: "water", mode: "mutable" },
];

aster.ASPECTS = [
    { id: 1, name: "conjunction", angle: 0, maxOrb: 8 },
    { id: 2, name: "opposite", angle: 180, maxOrb: 5 },
    { id: 3, name: "square", angle: 90, maxOrb: 5 },
    { id: 4, name: "trine", angle: 120, maxOrb: 5 },
    { id: 5, name: "sextile", angle: 60, maxOrb: 3 },
    { id: 6, name: "semisextile", angle: 30, maxOrb: 1.5 },
    { id: 7, name: "quincunx", angle: 150, maxOrb: 3}
];

const DEFAULT_PLANETS = [
    SC.SE_SUN, SC.SE_MOON, SC.SE_MERCURY, SC.SE_VENUS, SC.SE_MARS, SC.SE_JUPITER,
    SC.SE_SATURN, SC.SE_URANUS, SC.SE_NEPTUNE, SC.SE_PLUTO, SC.SE_MEAN_NODE
];

aster.getPlanets = function(date, location = null, planetIds = DEFAULT_PLANETS)
{
    if (date == null) throw new Error("Date is required to get planet positions");

    let planets = [];
    let houses = location ? this.getHouses(date, location) : null;
    planetIds.forEach(planet_id => {
        let planet = aster.getPlanetPosition(planet_id, date, houses);
        planets.push(planet);
    });

    return planets;
}

aster.getPlanetPosition = function(idOrName, date, houses = null)
{
    if (date == null) throw new Error("Date is required to get planet position");

    let planet = _.pick(aster.getPlanetDetails(idOrName), ['id', 'name']);
    planet.position = calculatePosition(planet.id, date);
    
    if (houses)
    {
        planet.house = calculateHouse(planet, houses.houses);
    }

    return planet;
}

aster.getPlanetDetails = function(idOrName)
{
    const id = parseInt(idOrName);
    const isId = !isNaN(id)
    let planets = this.PLANETS.filter(planet => isId ?
        planet.id == id :
        planet.name.toUpperCase() == idOrName.toUpperCase());

    if (planets.length == 1) return planets[0];
    else throw new Error(isId ?
        `Planet id ${id} not found.` :
        `Planet '${idOrName}' not found.`);
}

aster.getHouses = function(date, location, houseSystem = 'P')
{
    // swe_houses(julday, geolat, geolong, hsys)

    // latitude: northern is positive, southern is negative
    // longitude: eastern is positive, western is negative

    // hsys = P - Placidus
    //        K - Koch
    //        O - Porphyrius
    //        R - Regiomontanus
    //        C - Campanus
    //        A/E - Equal (cusp 1 is Ascendant)
    //        W - Whole sign

    if (date == null || location == null) {
        throw new Error("Date and location are required to get house cusp positions");
    }

    let julday = utcToJulianDate(date);
    let houses = sweph.houses(julday, location.lat, location.long, houseSystem);
    return {
        asc: degreeToPosition(houses.data.points[0]),
        mc: degreeToPosition(houses.data.points[1]),
        houses: houses.data.houses.map(degreeToPosition)
    };
};

aster.getAspects = function(planets, planets2)
{
    let isSingleChart = !planets2;
    if (isSingleChart) planets2 = planets;

    let aspects = [];

    for (var i = 0; i < planets.length; i++)
    {
        for (var j = isSingleChart ? i + 1 : 0; j < planets2.length; j++)
        {
            aspects = aspects.concat(calculateAspects(planets[i], planets2[j]));
        }
    }

    return aspects;
}

aster.getSummary = function(planets, aspects, houses)
{
    return calculateElementsAndModes(planets, houses);
}

const calculateAspects = function(planet1, planet2)
{
    if (!planet1 || !planet1.position || !planet2 || !planet2.position)
    {
        throw new Error("Two planets with positions must be provided to calculate aspect");
    }

    let aspects = [];
    for (var a = 0; a < aster.ASPECTS.length; a++)
    {
        let position1 = planet1.position.longitude;
        let position2 = planet2.position.longitude;
        let diff = Math.abs(position1 - position2);
        let aspect = aster.ASPECTS[a];
        if (diff >= aspect.angle - aspect.maxOrb &&
            diff <= aspect.angle + aspect.maxOrb)
        {
            aspects.push({
                planet1: planet1.id,
                planet2: planet2.id,
                aspect: _.pick(aspect, ['id', 'name']),
                orb: diff - aspect.angle });
        }
    }

    return aspects;
}

const calculatePosition = function(planetId, date = new Date(), minimal = false)
{
    const LONGITUDE = 0;
    const SPEED = 3;
    const FLAG = SC.SEFLG_SPEED | SC.SEFLG_SWIEPH;

    let julday = utcToJulianDate(date);
    let body = sweph.calc_ut(julday, planetId, FLAG);

    if (minimal)
    {
        return body.data[LONGITUDE];
    }
    else
    {
        let position = degreeToPosition(body.data[LONGITUDE]);
        position.speed = body.data[SPEED];
        position.retrograde = position.speed < 0;
        return position;
    }
};

const calculateHouse = function(planet, houses) {
    const planetPos = planet.position.longitude;

    for (let i = 0; i < houses.length; i++)
    {
        const housePos = houses[i].longitude;
        const nextHousePos = houses[(i + 1) % 12].longitude;

        if (housePos < nextHousePos)
        {
            if (planetPos >= housePos && planetPos < nextHousePos)
            {
                return i + 1;
            }
        }
        else if (planetPos >= housePos || planetPos < nextHousePos)
        {
            return i + 1;
        }
    }

    throw new Error("Error calculating hosue position of planet");
}

const calculateElementsAndModes = function(planets, houses)
{
    let elements = { earth: 0, air: 0, fire: 0, water: 0 };
    let modes = {cardinal: 0, fixed: 0, mutable: 0 };

    planets.forEach(planet =>
    {
        var sign = aster.SIGNS.find(sign => sign.id == planet.position.sign.id);
        elements[sign.element]++;
        modes[sign.mode]++;
    });

    if (houses)
    {
        var ascSign = aster.SIGNS.find(sign => sign.id == houses.asc.sign.id);
        var mcSign = aster.SIGNS.find(sign => sign.id == houses.mc.sign.id);

        elements[ascSign.element]++;
        elements[mcSign.element]++;
        modes[ascSign.mode]++;
        modes[mcSign.mode]++;
    }

    return { elements, modes };
}

const utcToJulianDate = function(date)
{
    let julday = sweph.julday(
        date.getUTCFullYear(),
        date.getUTCMonth() + 1,
        date.getUTCDate(),
        date.getUTCHours(),
        SC.SE_GREG_CAL);

    let houroffset = (((
        date.getUTCMilliseconds() / 1000 +
        date.getUTCSeconds()) / 60 +
        date.getUTCMinutes()) / 60) / 24;

    julday += houroffset;

    return julday;
};

const degreeToPosition = function(degree)
{
    let h = degree / 30;
    let d = degree - Math.floor(h) * 30;
    let m = (d - Math.floor(d)) * 60;
    let s = (m - Math.floor(m)) * 60;

    return {
        longitude: degree,
        sign: _.pick(aster.SIGNS[Math.floor(h)], ['id', 'name']),
        degree: Math.floor(d),
        minute: Math.floor(m),
        second: Math.floor(s)
    }
}

export default aster;