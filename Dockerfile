# syntax=docker/dockerfile:1
   
FROM node:20-slim
RUN apt-get update || : && apt-get install -y --no-install-recommends build-essential python3
ENV NODE_ENV production
WORKDIR /app
COPY --chown=node:node . .
RUN yarn install --production
CMD ["node", "api/server.js"]
USER node
EXPOSE 3000
