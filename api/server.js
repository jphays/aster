import aster from "../src/aster.js";
import restify from "restify";
import corsMiddleware from "restify-cors-middleware2";

let server = restify.createServer({
    name: "aster"
});

const cors = corsMiddleware({})

server.pre(restify.plugins.pre.sanitizePath());
server.pre(cors.preflight)
server.use(restify.plugins.queryParser());
server.use(cors.actual)

server.get("/chart", function(req, res, next)
{
    let date = req.query.date ? new Date(req.query.date) : new Date();
    let location = (req.query.lat && req.query.long) ?
        { lat: parseFloat(req.query.lat), long: parseFloat(req.query.long) } :
        null

    let houses = location ? aster.getHouses(date, location) : null;
    let planets = aster.getPlanets(date, location);
    let aspects = aster.getAspects(planets);
    let summary = aster.getSummary(planets, aspects, houses);
    res.send({ planets, aspects, houses, summary });
    return next();
});

server.get("/planets", function(req, res, next)
{
    let date = req.query.date ? new Date(req.query.date) : new Date();
    let planets = aster.getPlanets(date);
    res.send({ planets: planets });
    return next();
});

server.get("/planet/:name", function(req, res, next)
{
    let date = req.query.date ? new Date(req.query.date) : new Date();
    let planet = aster.getPlanetPosition(req.params.name, date);
    res.send(planet);
    return next();
});

server.get("/houses", function(req, res, next)
{
    let date = req.query.date ? new Date(req.query.date) : new Date();

    // seattle: 47.6062° N, 122.3321° W
    let lat = parseFloat(req.query.lat) || 47.6062;
    let long = parseFloat(req.query.long) || -122.3321;

    let houseData = aster.getHouses({ lat, long }, date);
    res.send({
        asc: houseData.asc,
        mc: houseData.mc,
        houses: houseData.houses
    });
    return next();
});

const port = 3000;
server.listen(port, function() {
    console.log('[restify] %s listening at %s', server.name, server.url);
});
